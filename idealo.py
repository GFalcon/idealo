#!/usr/bin/env python

import asyncio
import tempfile

import pyppeteer
from llog import log


async def launch(headless: bool):
  import pyppeteer
  # https://stackoverflow.com/questions/57987585/puppeteer-how-to-store-a-session-including-cookies-page-state-local-storage
  identity_dir = tempfile.gettempdir() + '/.chrome'
  browser = await pyppeteer.launch({'headless': headless, 'userDataDir': identity_dir})
  page = await browser.newPage()

  # wrap the “disconnected” hander in asyncio future
  disconnected = asyncio.get_event_loop().create_future()

  @browser.on('disconnected')
  def on_disconnected():
    disconnected.set_result(None)

  @page.on('close')
  def on_close():
    asyncio.get_event_loop().create_task(browser.close())

  return browser, page, disconnected


async def main():
  headless = False
  browser, page, disconnected = await launch(headless)
  await page.goto('https://www.idealo.de/preisvergleich/OffersOfProduct/6296589_-eq-9-plus-siemens.html',
                  {'waitUntil': 'domcontentloaded'})

  await page.waitForSelector ('h1.oopStage-title')

  title = await readInnerText(page, None, 'h1.oopStage-title')
  print('Title:', title)

  category = await readInnerText(page, None, 'span.oopStage-productInfoTopItem')
  print('Category:', category)

  details = await readDetails(page)
  print('Details:', details)

  productText = await readInnerText(page, None, '.editorialProductText .textris div')
  print('Product Text:', productText)

  await openAllOffers (page)

  offers = await readOffers(page)
  print('Offers:', offers)

  if not headless:
    await disconnected
  await browser.close()


async def readInnerText(page: pyppeteer.page.Page, context: pyppeteer.element_handle.ElementHandle,
                        selector: str):
  elem = await (page if context == None else context).querySelector(selector)
  value = await page.evaluate('elem => elem.innerText', elem)
  return value.strip()


async def readDetails(page: pyppeteer.page.Page):
  details = {}

  items = await page.querySelectorAll('ul.datasheet-list li.datasheet-listItem--properties')
  for item in items:
    key = await readInnerText(page, item, 'span.datasheet-listItemKey')
    value = await readInnerText(page, item, 'span.datasheet-listItemValue')
    details[key] = value

  return details


async def openAllOffers (page: pyppeteer.page.Page):
  # Click the "Weitere Angebote anzeigen" button until all shops and prices are displayed
  while True:
    try:
      # Find the button
      load_more = await page.waitForXPath ('''
          //button [
            contains(concat(' ',normalize-space(@class),' '),' productOffers-listLoadMore ')
            and
            contains(text(), 'Weitere Angebote anzeigen')
          ]
        ''', 
        {'timeout': 3000})
    except:
      # If it didn't appear in 3 seconds then all shops/prices are already displayed
      break
    # Click the button
    await load_more.click ()
    # Wait until it change its text
    await page.waitForXPath ('''
          //button [
            contains(concat(' ',normalize-space(@class),' '),' productOffers-listLoadMore ')
            and
            not(contains(text(), 'Weitere Angebote anzeigen'))
          ]
        ''')
  



async def readOffers(page: pyppeteer.page.Page):
  offers = []

  items = await page.querySelectorAll('.productOffers ul.productOffers-list li.productOffers-listItem')
  for item in items:
    a = await item.querySelector('a.productOffers-listItemTitle')
    title = await page.evaluate('elem => elem.innerText', a)
    href = await page.evaluate('elem => elem.href', a)

    price = await readInnerText(page, item, 'a.productOffers-listItemOfferPrice')

    img = await item.querySelector('img.productOffers-listItemOfferLogoShop')
    shop = await page.evaluate('elem => elem.alt', img)

    offers.append((title, href, price, shop))

  return offers


if __name__ == '__main__':
  loop = asyncio.new_event_loop()
  asyncio.set_event_loop(loop)
  loop.run_until_complete(main())
